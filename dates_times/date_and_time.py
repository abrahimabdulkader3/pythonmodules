from datetime import datetime

datetime(1941, 5, 24, 1, 30)
str(datetime(1941, 5, 24, 1, 30))


# my_birthday = datetime(1999, 9, 16, 1, 30)
# print(my_birthday)


# right_now = datetime.now()
# assert right_now == datetime.now()

today = datetime(2022, 10, 11, 1, 11, 11)
today.strftime("%y-%m-%d %H:%M:%S")
print(today)
